# README #

**WeakIV** is a Stata program that computes weak identification statistics after instrumental variables regressions. 
It currently only computes the Cragg-Donald minimum eigenvalue statistic. 
It functions after **ivregress**, **xtivreg**, or **reghdfe** including first stage regressions using factor variable syntax.

# Installation #
You can install the latest version of the package directly from this repository using the following in Stata:


```
#!Stata

net install WeakIV, from("https://bitbucket.org/drofphilosophe/stata.weakiv/raw/master") replace
```
Syntax and usage details are included in the associated Stata help file. 

### Contribution ###
Contributions to this project are welcome. If you are interested in contributing, contact the project administrator. Particularly helpful contributions include:

* Adding additional tests of weak identification, including the Kleinberg-Papp rk statistic. 
* Expanding support to additional IV estimators
* Compute weak identification p-values and confidence intervals on finite sample bias

# License #

Copyright 2016 James Archsmith

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.