/******************************************************
******************************************************
** WeakIV
**

Copyright 2016 James Archsmith

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

**
** This program implements one or more test of weak or under-identified instrumental
** variables on theactive regression results. The program should work for both 
** the standard IV and panel IV commands and should accept regression specifications
** using factor variable or time-series syntax. 
*****************************************************************/



mata
	/* Mata function takes endogenous regressors, exogeneous instruments, and 
	   exogeneous covariates as arguments and returns the Cragg Donald F statistic 
	   The test is based on Stock and Yogo (2001) Section 2.3 */
	real scalar function CraggDonald(real matrix X, /*matrix of endogenous regressors */
									real matrix Z, /*Matrix of exogeneous instruments */
									real matrix A,	/*Matrix of exogeneous covariates */
									real scalar dfadj	/* Degrees of freedom adjustment, 
															accounts for lost DF when we have partialed out fixed effects */ ) {
	
		/* Number of observations */
		T = rows(X)
		/* Number of instruments */
		K = cols(Z)
		/* Number of exogeneous regressors */
		K1 = cols(A) + dfadj
				
		/* Partial A out of X and Z */
		invAA = invsym( quadcross(A,A) )
		B = quadcross(invAA, quadcross(A,X) )
		X = X - quadcross(A',B)
		B = quadcross(invAA, quadcross(A,Z) )
		Z = Z - quadcross(A',B)
		
		/* Compute regression coefficients and resiuals of regressions of X on Z 
			(Z'Z)^-1*Z'X */
		PI = quadcross( invsym( quadcross(Z,Z) ) , quadcross(Z,X) )
		U = X - quadcross(Z',PI)
		
		/* Compute V[U]. Need to make the finite sample adjustment */
		V = quadcross(U,U):/(T - K - K1)
			
		/* I guess S can be costly to compute. Do it once */
		S = luinv(cholesky(V))
		
		/* Compute F-analog matrix Start my computing Z*PI*S' */
		ZPIS = quadcross(Z',quadcross(PI',S'))
		F = quadcross(ZPIS,ZPIS)
		/*F = quadcross(Z*PI*S',Z*PI*S'):/K */
		
		/* The Cragg-Donald statistic is the minimum of eigenvalues of F */
		/* F is positive definite, so it's eigenvalues are all real */
		cdf = min(symeigenvalues(F))

		/* Finite sample adjustment */
		cdf = cdf * (T-K1-1)/(T-K-K1)
		
		return( cdf )
	}
end

capture program drop fvconvert
program define fvconvert , rclass
	syntax varlist(fv), prefix(string)
	di
	di "{txt}Expanding varlist {res}`varlist'"
	**Take the varlist and xi all the factor variables
	fvexpand `varlist'
	local fvlist `r(varlist)'
	
	capture drop `prefix'*
	**Now filter all the factor variables out of `varlist'
	local nofv
	local fv
	local vc 0
	foreach v of local fvlist {
		if strpos("`v'",".") == 0 {
			local nofv : list nofv | v
		}
		else {
			local vc `=`vc'+1'
			qui gen `prefix'`vc' = `v'
		}
	}
	
	capture desc `prefix'*, varlist
	if _rc != 0 {
		di "{txt}Warning: No factor variables in varlist"
		local fv
	}
	return local nofv `nofv'
	return local fv `r(varlist)'
end

*************************************
** Stata Wrapper program 
** it is designed to be called after stata IV commands 
*************************************
capture program drop WeakIV
program define WeakIV, rclass

	tempvar touse
	gen byte `touse' = e(sample)
	
	tempname X Z A cdf idstat idp dfadj
	
	/* I assume every ivreg command has the same varlist syntax
	   <depvar> [covariates] (<endog> = <exog>) [covariates] ,
	   and returns the same macros as xtivreg and ivregress
	*/
	local endog `e(instd)'
	if regexm("`e(cmdline)'", "\(([A-Za-z0-9_\.\ ]+)=([A-Za-z0-9_#\.\ ]+)\)") {
		local exog `=regexs(2)'
	}
	fvexpand `exog'
	local exog `r(varlist)'
	fvexpand `e(insts)'
	local vlist `r(varlist)'
	local covars : list vlist - exog

	di 
	di "{txt}Weak IV Statistics"
	di
	di "{txt}Endogenous regressors: {res}`endog'"
	di "{txt}Exogenous instruments: {res}`exog'"
	di "{txt}Exogeneous covariates: {res}`covars'"
	di
	
	preserve
		qui keep if `touse'
		/* Partialing out the fixed effects here is probably faster than doing it in Mata */
		if "`e(cmd)'" == "xtivreg" {
			di "{txt}Panel IV estimatior"
			
			if "`e(model)'" == "fe" {
				local panelmodel fe
			}
			else if "`e(model)'" == "g2sls" {
				local panelmodel re
				local otherargs ratio(`=e(rho)')
			}
			else {
				di "{err}Unknown panel data estimator {res}`e(model)'"
				exit 99
			}
			
			**Extract the number of groups. You need this for the degrees of 
			**freedom adjustment since we use xtdata
			local dfadj `=e(N_g)'
			
			tempvar cons
			qui gen byte `cons' = 1
			
			qui fvconvert `exog', prefix(_I)
			local exog `r(fv)' `r(nofv)'
		
			qui fvconvert `endog', prefix(_J)
			local endog `r(fv)' `r(nofv)'
			
			local covars `covars' `cons'
			_rmcoll `covars' , nocons
			qui fvconvert `r(varlist)', prefix(_K)
			local covars `r(fv)' `r(nofv)'

		
			qui xtdata `exog' `endog' `covars', `panelmodel' `otherargs' clear

			**Assume instruments are not collinear with any covariates.
			**If so the previous regression would have failed. Why do we check
			**for collinearity? The added constant term may be collinear with an
			**existing variable
		}
		else if "`e(cmd)'" == "reghdfe" {
			local panelmodel fe
			local panelvar `e(absvars)'
			local pvcount : word count `panelvar'
			if `pvcount' > 1 {
				di "{err}Multiple abosrb variables specified in {bf:absorb(}{res}`panelvar'{err}{bf:)}."
				di "WeakIV can only abosrb one variable."
				exit 99
			}
			**Neet to determine the number of absorbed levels
			**e(sample) from reghdfe marks out any singleton absorbed
			**levels. So wee need to remove these from the degrees of 
			**freedom adjustment]
			**K1 is the number of levels in the first absorb var
			**M1 is the number of redundant levels in the first aborb var
			**Only use M1 and K1 becuase the program will fail earlier
			**if we specified multiple absorb vars
			local dfadj = e(K1) - e(M1)
			
			tempvar cons
			qui gen byte `cons' = 1
			
			qui fvconvert `exog', prefix(_I)
			local exog `r(fv)' `r(nofv)'
		
			qui fvconvert `endog', prefix(_J)
			local endog `r(fv)' `r(nofv)'
			
			local covars `covars' `cons'
			_rmcoll `covars' , nocons
			qui fvconvert `r(varlist)', prefix(_K)
			local covars `r(fv)' `r(nofv)'

		
			qui xtdata `exog' `endog' `covars', `panelmodel' `otherargs' clear			
					
		}
		else {
			di "{txt}Other estimator"
			
			**Zero degrees of freedom adjustment
			local dfadj 0
			
			/* We we don't partial out fixed effects, we need to add a constant to the covariates */
			tempvar cons
			qui gen byte `cons' = 1
			local covars `covars' `cons'
			**Assume instruments are not collinear with any covariates.
			**If so the previous regression would have failed. Why do we check
			**for collinearity? The added constant term may be collinear with an
			**existing variable
			_rmcoll `covars' , nocons expand
			local covars `r(varlist)'
		}
		mata:`X' = st_data(.,"`endog'")
		mata:`Z' = st_data(.,"`exog'")
		mata:`A' = st_data(.,"`covars'")
		mata:`cdf'=CraggDonald(`X',`Z',`A',`dfadj')
		mata:st_numscalar("`cdf'", `cdf')
		
		di
		/**Also perform the underidentification test */
		/*qui ranktest (`endog') (`exog'), partial(`covars') robust
		scalar `idstat' = r(chi2)
		scalar `idp' = r(p)
		
		di
		di "{txt}Underidentification test:"
		di "{txt}Kleibergen-Paap rk LM statistic: {res}" `idstat'
		di "{txt}p-value: {res}" `idp'
		di*/
		di "{txt}Weak Identification test:"
		di "{txt}Cragg-Donald F statistic: {res}" `cdf'
	restore
	return scalar cdf = `cdf'
	/*return scalar idstat = `idstat'
	return scalar idp = `idp'*/
end

