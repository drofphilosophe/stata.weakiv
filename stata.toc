*Table of contents for the WeakIV Stata ADO

*Version (3 is the most recent for TOC files)
v 3
d James Archsmith, University of California, Davis
d http://www.econjim.com
p WeakIV Compute weak identification statistics after instrumental variables regression commands
