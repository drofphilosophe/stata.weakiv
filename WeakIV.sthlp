{smcl}
{* *! version 1.2.1  07mar2013}{...}
{findalias asfradohelp}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[R] help" "help help"}{...}
{viewerjumpto "Syntax" "WeakIV##syntax"}{...}
{viewerjumpto "Description" "WeakIV##description"}{...}
{viewerjumpto "Options" "WeakIV##options"}{...}
{viewerjumpto "Remarks" "WeakIV##remarks"}{...}
{viewerjumpto "Examples" "WeakIV##examples"}{...}
{viewerjumpto "Stored Results" "WeakIV##storedresults"}{...}
{viewerjumpto "References" "WeakIV##references"}{...}
{title:Title}

{phang}
{bf:WeakIV} {hline 2} Compute weak instrumental variables statistics after regression commands.


{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:WeakIV}

{marker description}{...}
{title:Description}

{pstd}
{cmd:WeakIV} computes weak instrumental variables statistics on the estimation results 
currently in memory. 

{marker remarks}{...}
{title:Remarks}

{pstd}
This program currently only provdes the Cragg-Donald F statistic of weak identification. 
It works after running {cmd:ivregress} or {cmd:xtivreg} and should accept regression 
specifications using factor variable or time series syntax. Other user-contributed 
programs (notabily {cmd:ivreg2}) compute a wider array of statistics, but don't support
panel data estimators or factor variables.
{p_end}

{pstd}
This program is licensed under the Apache License, Version 2.0. 
See {browse "http://www.apache.org/licenses/LICENSE-2.0":www.apache.org/licenses/LICENSE-2.0}.
{p_end}

{pstd}
Calculation of the Cragg-Donald Statistic is based on Stock and Yogo (2001).
{p_end}

{marker examples}{...}
{title:Examples}

{pstd}Setup{p_end}
{phang2}{cmd:. webuse hsng2}{p_end}

{pstd}Fit an IV regression{p_end}
{phang2}{cmd:. ivregress 2sls rent pcturban (hsngval = faminc i.region), small}{p_end}

{pstd}Obtain weak IV statistics{p_end}
{phang2}{cmd:. WeakIV}{p_end}

{marker storedresults}{...}
{title:Stored results}

{pstd}
{cmd:WeakIV} stores the following in {cmd:r()}:

{synoptset 20 tabbed}{...}
{p2col 5 20 24 2: Scalars}{p_end}
{synopt:{cmd:r(cdf)}}Cragg-Donald F weak identification statistic{p_end}



{marker references}{...}
{title:References}

{marker A1991}{...}
{phang}
Stock, J.H. and Yogo, M., 2005. Testing for weak instruments in linear IV regression. 
	Identification and inference for econometric models: Essays in honor of Thomas Rothenberg.
{p_end}
