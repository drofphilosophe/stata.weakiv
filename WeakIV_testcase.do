***********************************
** Test code to compare my results against ivreg2
** and test functionality
************************************ 

clear all
*set seed 1234567890
set obs 2000
set matsize 11000
set more off

do "I:\Personal Files\Jim\Stata\Git\WeakIV\WeakIV.ado"
local XZ .5
local cE .05
local c0 0
local c1 1
local XX 0.05
local ZZ 0.05
#delim ;
	drawnorm x1 x2 z1 z2 e, 
		cov(
			`c1' `XX' `XZ' `XZ' `cE' \ 
			`XX' `c1' `XZ' `XZ' `cE' \
			`XZ' `XZ' `c1' `ZZ' `c0' \
			`XZ' `XZ' `ZZ' `c1' `c0' \
			`cE' `cE' `c0' `c0' `c1'
		)
	;
#delim cr

gen a = runiformint(1,5)
gen b = runiformint(1,5)
gen y = x1 + x2 + a + b + e

***************
** Standard IV
***************
ivreg2 y (x1 x2 = z1 z2) i.b, robust
scalar cdf = e(cdf) 
ivregress 2sls y (x1 x2 = z1 z2) i.b, robust
WeakIV

di "{txt}My rk: " r(cdf)
di "{txt}Their CD: " cdf
di r(cdf)/cdf


****************
** Fixed Effects estimator
****************
xtset a
xtivreg y (x1 x2 = z1 z2) i.b , fe
WeakIV

***********************
** reghdfe
***********************
reghdfe y i.b (x1 x2 = z1 z2), absorb(a) vce(cluster a)
WeakIV

****************
** Random Effects estimator
****************
xtset a
xtivreg y (x1 x2 = z1 z2) i.b , re
WeakIV



